package HashMap;

import java.util.HashMap;


public class WordCounter {
	private String str;
	private HashMap<String, Integer> wordCount;
	
	public WordCounter(String str){
		this.str = str;
		wordCount = new HashMap<>();
	}
	
	public void count(){
		String[] list = str.split(" ");
		for (String message : list){
			if (wordCount.containsKey(message)){
				wordCount.put(message, wordCount.get(message)+1);
			}
			else{
				wordCount.put(message, 1);
			}
		}
	}
	
	public int hasWord(String word){
		if (wordCount.containsKey(word)){
			return wordCount.get(word);
		}
		else{
			return 0;
		}
	}
}
