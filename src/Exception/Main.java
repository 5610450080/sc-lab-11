package Exception;

import java.util.Scanner;

public class Main {
	public static void main(String[] args){
		Refrigerator rf = new Refrigerator();
		try{
			rf.put("Milk");
			rf.put("Cookies");
			rf.put("Apple");
			rf.put("Orange");
			rf.put("Cake");
			System.out.println("-------STUFF-------(before takeout)\n"+rf.toString());
			
			rf.takeOut("Milk");
			rf.takeOut("Cake");
			System.out.println("-------STUFF-------(after takeout)\n"+rf.toString());
		
			
		}
		catch (FullException e){
			System.err.println("Error : "+e.getMessage());
		}
	}
}
	
		
		
		
