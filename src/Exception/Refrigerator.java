package Exception;

import java.util.ArrayList;

import Exception.FullException;

public class Refrigerator {
	private int size;
	private ArrayList<String> things;
	
	public Refrigerator(){
		this.size = 5;
		things = new ArrayList<String>();
	}
	
	public void put(String stuff) throws FullException{
		if (things.size() < size){
			things.add(stuff);
		}
		else if (things.size() >= 5){
			throw new FullException("Refrigerator are FULL please take out things.");
		}
	}
	
	public String takeOut(String stuff){
		String st = "";
		if (things.contains(stuff)){
			st = stuff;
			things.remove(stuff);
		}
		else{
			st = null;
		}
		return st;
			
	}
	
	public String toString(){
		String list = "";
		for (String s : things){
			list += "Stuff in Refrigerator : "+ s +"\n";
		}
		return list;
		
	}
	

}
